import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import { Route, NavLink} from "react-router-dom";
import TodoList from "./TodoList";
import { connect } from "react-redux";
import { clearCompletedTodos, addTodo, deleteTodo, toggleTodo } from "./actions";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };
  
  
  handleDeleteTodo = id => event => {
    this.props.deleteTodo(id)
  };
  handleChangeCompleted = id => event => {
    this.props.toggleTodo(id)
  };
  handleCreateTodo = event => {
    if(event.key === "Enter") {
      this.props.addTodo(this.state.value)
      this.setState({
        value: ""
      });
    }
  };
  handleChange = event => {
    this.setState({ value: event.target.value });
  };
  
  //==================================================
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            onChange={this.handleChange}
            value={this.state.value}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route 
          exact path="/"
          render= {() => (
            <TodoList 
              todos={this.props.todos} 
              handleChangeCompleted={this.handleChangeCompleted}
              handleDeleteTodo={this.handleDeleteTodo} 
              />
            )}
        />

        <Route 
          path="/active"
          render= {() => (
            <TodoList 
              todos={this.props.todos.filter(todo => !todo.completed)} 
              handleChangeCompleted={this.handleChangeCompleted}
              handleDeleteTodo={this.handleDeleteTodo} 
              />
            )}
        />

        <Route 
          path="/completed"
          render= {() => (
            <TodoList 
              todos={this.props.todos.filter(todo => todo.completed)} 
              handleChangeCompleted={this.handleChangeCompleted}
              handleDeleteTodo={this.handleDeleteTodo} 
              />
            )}
        />


        <footer className="footer">
          <span className="todo-count">
            <strong>
              {this.props.todos.filter(todo => !todo.completed).length}
            </strong> 
            {" item(s) left"}
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>

          <button 
            className="clear-completed" 
            onClick={this.props.clearCompletedTodos}>Clear completed
          </button>

        </footer>
      </section>
    );
  };
}
//so we can use this.props.todos
const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = {
  clearCompletedTodos,
  addTodo,
  deleteTodo,
  toggleTodo
};

export default connect( mapStateToProps, mapDispatchToProps )(App);

