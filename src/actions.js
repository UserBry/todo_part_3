export const TOGGLE_TODO = 'TOGOLE_TODO';
export const ADD_TODO = 'ADD_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const CLEAR_COMPLETED_TODOS = 'CLEAR_COMPLETED_TODOS';


export const addTodo = value => {
    return {
        type: ADD_TODO,
        payload: {
            userId: 1,
            id: Math.floor(Math.random() * 1000000),
            title: value,
            completed: false
        }
    };
};

export const deleteTodo = id => {
    return {
        type: DELETE_TODO,
        payload: id
    };
};

export const toggleTodo = id => {
    return {
        type: TOGGLE_TODO,
        payload: id
    };
};

export const clearCompletedTodos = () => {
    return {
        type: CLEAR_COMPLETED_TODOS
    }
}
