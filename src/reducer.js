import todosList from "./todos.json";
import { CLEAR_COMPLETED_TODOS, ADD_TODO, 
         DELETE_TODO, TOGGLE_TODO} from "./actions.js";

const initialState = {
    todos: todosList,
};
 
const reducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_TODO:
            return { 
                ...state,
                todos: [...state.todos, action.payload]
            };

        case DELETE_TODO:
            return {
                ...state,
                todos: state.todos.filter(todo => todo.id !== action.payload)
            };

        case TOGGLE_TODO:
            return {
                ...state,
                todos: state.todos.map(todo => {
                            if(todo.id === action.payload) {
                                return {
                                    ...todo,
                                    completed: !todo.completed
                                };
                            }
                            return todo;
                        })
            }

        case CLEAR_COMPLETED_TODOS:
            return { 
                    ...state,
                    todos: state.todos.filter(todo => todo.completed !== true) 
                };
        default:
            return state;
    }
};

export default reducer;